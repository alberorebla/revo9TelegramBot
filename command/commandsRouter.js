/**
 * Created by aalberini on 03/02/16.
 * Classe di routing per la gestione di tutti i comandi in ingresso
 *
 */

var CommandsList = require('./commandList');
var validCommands = CommandsList.validCommands;
var botx;
function defineCommand(outerCommand) {
    var command=outerCommand.cmd;
    botx.onText(
        command.getRegex(),
        function(msg, match){
            command.callback(msg, match, botx)
        });
}

exports.defineCommandForBot=function(bot){
    botx = bot;
    validCommands.forEach(defineCommand);
};
/**
 * Created by aalberini on 03/02/16.
 */

var Command = require('../command');
var cmdOpt = {
    hasAttribute:true
    ,needAuthorization:true
};
exports.cmd = new Command("/teamspeak",
    function(msg, match, bot){
        var fromId = msg.from.id;
        var mesOut;
        switch (match[1]) {
            case "ip":
                mesOut="IP:\trevolution9.clants.net\nPorta:\t3640"
                break;
            default:
                mesOut="Parametri supportati: ip"
        }
        bot.sendMessage(fromId, mesOut);
    }, cmdOpt);
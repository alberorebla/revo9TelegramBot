/**
 * Created by aalberini on 03/02/16.
 *
 * La funzione è molto lenta, forse sarebbe il caso di capire come evitare il flood del comando.
 * Per evitare di aspettare si potrebbe:
 *  - ridurre i file, sotto il mega
 *  - aggiungere i file in qualche repository online a banda infinita
 *
 */

var Command = require('../command');
var fs = require('fs');
var request = require('request');
var cmdOpt = {
    hasAttribute:true
    ,needAuthorization:true
};

var audioRep ={
    zioIsBurning: "/home/aalberini/WebstormProjects/Hermes_PTBot/command/media/audio/zio_is_burning.mp3"
};

exports.cmd = new Command("/revosound",
    function(msg, match, bot){
        var chatId = msg.chat.id;
        var opt = match[1];
        var mesOut;
        if (audioRep.hasOwnProperty(opt)){
            bot.sendAudio(chatId, audioRep[opt]);
        }else{
            mesOut="Funzione non presente";
            bot.sendMessage(chatId, mesOut);
        }
    }, cmdOpt);
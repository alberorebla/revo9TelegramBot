/**
 * Created by aalberini on 03/02/16.
 */
var rules = require('./../lib/revo9words');
var Command = require('../command');
var options = {
    parse_mode:"Markdown"
};
var cmdOpt = {
    hasAttribute:true
    ,needAuthorization:true
};
var waitResponse;
exports.cmd = new Command("/statutox",
    function(msg, match, bot){
        var fromId = msg.from.id;
        var chatId = msg.chat.id;
        var msgId = msg.message_id;
        var ruleKey = match[1];
        var mesOut;
        if (ruleKey==undefined){
            mesOut = "Le regole dello statuto sono: " + rules.getAvabileRules();

            options["reply_markup"]=JSON.stringify({
                force_reply:true,
                keyboard: [
                     ['1','2','3','4']
                    ,['5','6','7','8']
                    ,['9','10','11','12']
                    ,['elena']
                ],selective:true
            });
            options.reply_to_message_id=msgId;
            bot.sendMessage(chatId, "Scegliere la regola", options)
                .then(function (sended) {
                    var chatId = sended.chat.id;
                    var messageId = sended.message_id;
                    bot.onReplyToMessage(chatId, messageId, function (resp) {
                        mesOut = "*Regola "+ resp.text +"#* - "+rules.getRule(resp.text);

                        options.parse_mode="Markdown";
                        options.reply_markup=JSON.stringify({
                            hide_keyboard:true
                        });

                        bot.sendMessage(chatId, mesOut, options);
                    });
                })

        } else {
            mesOut = "*Regola "+ ruleKey +"#* - "+rules.getRule(ruleKey);
        }
    }, cmdOpt);
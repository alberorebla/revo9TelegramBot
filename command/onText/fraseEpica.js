/**
 * Created by aalberini on 03/02/16.
 */
var ElencoFrasi = require('./../lib/frasiEpiche');
var Command = require('../command');
var options = {
    parse_mode:"Markdown"
};
var cmdOpt = {
    hasAttribute:false
    ,needAuthorization:true
};
exports.cmd = new Command("/fraseepica",
    function(msg, match, bot){
        var fromId = msg.from.id;
        var chatId = msg.chat.id;
        var mesOut = ElencoFrasi.getFraseRandom();
        bot.sendMessage(chatId, mesOut, options);
    }, cmdOpt);
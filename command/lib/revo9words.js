/**
 * Created by aalberini on 03/02/16.
 *
 * Regolamento ufficiale Revo 9
 * Le regole sarebbe più sensato spostarle su db ed evitare di scriverle qui
 */

exports.getRule = function( number ){
    try {
        return rules[number]
    } catch (exception) {
        return "Regola non trovata, Stai forse cercando una falla dello statuto?"
    }
};

exports.getAvabileRules = function( number ){
    return Object.keys(rules);
};

//TODO studiare un sistema per recuperare le informazioni da DB

var rules = {
     1: "NON e' consentito pronunciare e scrivere la parola proibita, ne' alludere minimamente ad essa con alliterazioni o assonanze. E' proibita anche la diffusione di immagini che riproducano tale parola."
    ,2: "E' proibito accusare Venom di non saper gestire un forum o un sito. E' proibito anche fare allusioni con scritte, immagini, suoni e odori."
    ,3: "Non E' ammesso INSISTERE a cantare, fischiettare o richiamare in qualsiasi modo canzoni di qualsivoglia gruppo o pseudocantante. (I Tokio Hotel in particolare)"
    ,4: "E' proibito parlare, scrivere, musicare su WoW (E' anche proibito creare canali)"
    ,5: "E' Vietato entrare in ts e farsi i cazzi propri senza nemmeno salutare."
    ,6: "E' Proibito trovare e sfruttare le falle dello statuto"
    ,7: "E comunque lo statuto non ha falle, E' invincibile"
    ,8: "No homo, no emo"
    ,9: "Regola jolly. (utilizzabile SOLAMENTE 2 volte in un ora)"
    ,10: "Ogni kick necessita di una motivazione contenuta nello statuto."
    ,11: "Non E' ammesso INSISTERE a produrre rumori molesti di qualsiasi genere che possano dare fastidio"
    ,12: "Vietato accusare loki di non saper fare le cfg, o che le sue cfg fanno schifo"
    ,elena:"E' ammesso kikkarla per qualsiasi ragione"
}












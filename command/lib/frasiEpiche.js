/**
 * Created by aalberini on 07/02/16.
 *
 * Le frasi sono da mettere su un db, nosql sarebbe meglio
 */

exports.getFraseRandom = function(){
      return elencoFrasi[Math.floor(Math.random()*elencoFrasi.length)];
};

var elencoFrasi=[
    "*Ste*: \"E questa \u00E8  la cicatrice a forma di F che ho sulla fronte\"\r\n*Lei*: \"F sta per frocio?\"\r\n*Ste*: \"No, sta per Francesca\" (l\'altra)"
,"\"Ne incontro una ogni 3 anni, va te se mi doveva capitare quella buggata\" *Venom*_ parlando delle sue avventure alla cieca_"
,"*Buuuuuuuurpleargh!! .... cazzo, mamma porta lo straccio! \" \r\n*Lestat* _dopo l\'ennesimo rutto da rendere l\'anima al demonio_"
,"\"Parli di cioccolato e mangi la merda!\" _Grandi dibattiti politici tra_ *Loki* _e lo_ *Zio*"
,"\"Nel quartiere di Gordon ci sono i pioppi che hanno le maniglie\" *Anonimo*"
,"\"Grazie per i fiori!\" *Venom* _dopo aver visto i cazzi tripallati disegnati sulla sua gomma nuova_"
,"*Loki*:\"Perchè i tuoi genitori ce l'hanno con te?\"\n*Zio*:\"Costo ma non produco!\" *Zio Sam* _vs genitori_"
,"*Elena*: \"...si e poi Alex mi ha fregato la sedia\"\r\n*Lestat*: \"ma avete una sedia sola in casa?\"\r\n*Albe*: \"a casa di Alex mettono su la musica e ad un certo punto si ferma e chi rimane in piedi viene eliminato\"\r\nLOL\r\n"
,"*Loki*:\"Cheppalle Alice, non mi lascia in pace\"\r\n*Lestat*: \"Gira le telefonate a me\"\r\n*Venom*: \"Eh ma lo devi imitare bene per\u00F2\"\r\n*Lestat*: \"aspetta: \"Minchia!\" , \"Minchia!\" , \"Ma veramente?\". ok sono pronto.\""
,"\"Loki, No Aim, No Gain\" *Venom*"
,"\"Qua c\'\u00E8 Lag di Ginocchio!\" *Albe*"
,"\"Loki \u00E8 uscita la pre-order di Guild Wars 2\"\r\n\"*AHHHHHHHHHHHHHHHHHHH,PAPAAAAAAAAAAAA\'!!!*"
,"*Ste*: \"Aca se siamo stronzi a fare commenti del genere su di lei, sai quanto ci sta male? E sai anche quanto non me ne frega un cazzo?\""
]
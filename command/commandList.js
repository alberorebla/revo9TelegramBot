/**
 * Created by aalberini on 03/02/16.
 *
 * lista di comandi disponibili
 *
 */

var normalizedPath = require("path").join(__dirname, "onText");
var cmdslist = [];

require("fs").readdirSync(normalizedPath).forEach(
    function(file) {
        cmdslist.push(require("./onText/" + file));
});

exports.validCommands=cmdslist;




/**
 * Created by aalberini on 03/02/16.
 *
 * Oggetto descrittore di un dato comando
 *
 */
var auth = require('./lib/authUser');
var Util = require('./util/utilFunction');
var option = {
    hasAttribute:true
    ,needAuthorization:true
}
module.exports = function Command (cliCommand, callback, option){
    this.cliCommand = cliCommand;
    this.callback = callback;
    var cbox;

    if (option!=undefined){
        this.option = option;
    }

    if (this.callback == undefined) {
        this.callback = function(){
            console.log("Funzione non abilitata");
        }
    }

    cbox=this.callback;
    this.callback = function(msg, match, bot){
        var fromId = msg.from.id;
        if (option.hasAttribute) {
            var completeMessage = msg.text;
            var cmd = completeMessage.substring(completeMessage.indexOf('/'), completeMessage.length);
            match = Util.getArguments(cmd, true);
        }

        if (option.needAuthorization) {
            if (!auth.isAuthorized(fromId)){
                var mesOut="Personale non autorizzato!L'incidente sarà riportato alle autorità competenti";
                console.log("Un folle ha provato a comandarmi %s",fromId)
                bot.sendMessage(fromId, mesOut);
                return;
            }
        }

        cbox(msg, match, bot);
    };

    this.getRegex = function (){
        return new RegExp(this.cliCommand)
    };



};

var TelegramBot = require('node-telegram-bot-api');
var Common = require('./variable');
var CommandsRouter = require('./command/commandsRouter');

var token = Common.token;

var bot = new TelegramBot(token, {polling: true});

CommandsRouter.defineCommandForBot(bot);
